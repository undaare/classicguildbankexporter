local ClassicGuildBankExporter = LibStub("AceAddon-3.0"):NewAddon("ClassicGuildBankExporter", "AceConsole-3.0", "AceEvent-3.0")
local ClassicGuildBankExporterGUI = LibStub("AceGUI-3.0")

local CHAT_COMMAND = "cgbe"

local SettingsWindowFrame = nil
ApiToken = ApiToken or ""
BankExportString = BankExportString or ""
GuildName = GuildName or ""

function ClassicGuildBankExporter:OnInitialize()
	ClassicGuildBankExporter:RegisterChatCommand(CHAT_COMMAND, 'HandleExportChatCommand')
	ClassicGuildBankExporter:RegisterEvent("BANKFRAME_OPENED")
    ClassicGuildBankExporter:RegisterEvent("BANKFRAME_CLOSED")
end

function ClassicGuildBankExporter:HandleExportChatCommand(input)
	input = strtrim(input)
	if input == "config" or not ClassicGuildBankExporter:HasApiToken() then
		ClassicGuildBankExporter:ToggleSettingsWindow()
	elseif input == "export" then
		ClassicGuildBankExporter:SaveExportString()
		ClassicGuildBankExporter:SaveGuildName()
	elseif input == "reload" then
		ClassicGuildBankExporter:Reload()
	else
		ClassicGuildBankExporter:Help()
	end
end

function ClassicGuildBankExporter:Help()
	ClassicGuildBankExporter:Print("Usage: /" .. CHAT_COMMAND .. " <config|export|reload>")
end

function ClassicGuildBankExporter:BANKFRAME_OPENED(eventName)
    ClassicGuildBankExporter.isBankOpen = true
end

function ClassicGuildBankExporter:BANKFRAME_CLOSED(eventName)
    if ClassicGuildBankExporter.isBankOpen and ClassicGuildBankExporter:HasApiToken() then
		ClassicGuildBankExporter:SaveExportString()
		ClassicGuildBankExporter:SaveGuildName()
    end
    ClassicGuildBankExporter.isBankOpen = false
end

function ClassicGuildBankExporter:CreateExportString()
	local bags = ClassicGuildBankExporter:GetBags()
	local bagItems = ClassicGuildBankExporter:GetBagItems()

	local exportString = '[' .. UnitName('player') .. ',' .. GetMoney() .. ',' .. GetLocale() .. '];'

	exportString = exportString .. '['

	for i=1, #bags do
		if i > 1 then
			exportString = exportString .. ','
		end

		exportString = exportString .. bags[i].container .. ',' 

		if bags[i].bagName == nil == false then
			exportString = exportString .. bags[i].bagName
		end
	end

	exportString = exportString .. '];'

	for i=1, #bagItems do
		exportString = exportString .. '[' .. bagItems[i].container .. ',' .. bagItems[i].slot .. ',' .. bagItems[i].itemID .. ',' .. bagItems[i].count .. '];'
	end
	return exportString
end


function ClassicGuildBankExporter:HasApiToken()
	if ApiToken == nil then
		return false
	end
	
	if ApiToken == "" then
		return false
	end
	
	return true
end

function ClassicGuildBankExporter:GetBags()
  local bags = {}

  for container = -1, 12 do
    bags[#bags + 1] = {
      container = container,
      bagName = GetBagName(container)
    }
  end

  return bags
end

function ClassicGuildBankExporter:GetBagItems()
  local bagItems = {}

  for container = -1, 12 do
    local numSlots = GetContainerNumSlots(container)

    for slot=1, numSlots do
      local texture, count, locked, quality, readable, lootable, link, isFiltered, hasNoValue, itemID = GetContainerItemInfo(container, slot)

      if itemID then
        bagItems[#bagItems + 1] = {                    
          container = container,
          slot = slot,
          itemID = itemID,
          count = count
        }
      end
    end
  end

  return bagItems
end

function ClassicGuildBankExporter:SaveExportString()
	local exportString = ClassicGuildBankExporter:CreateExportString()
	exportString = ClassicGuildBankExporter:EncodeExportString(exportString)
	BankExportString = exportString
	ClassicGuildBankExporter:Print("String has been exported.")
end

function ClassicGuildBankExporter:SaveGuildName()
	GuildName, _ = GetGuildInfo("player")
end

function ClassicGuildBankExporter:ToggleSettingsWindow()
	if ClassicGuildBankExporter:IsSettingsWindowOpen() then
		ClassicGuildBankExporter:CloseSettingsWindow()
	else
		ClassicGuildBankExporter:OpenSettingsWindow()
	end
end

function ClassicGuildBankExporter:IsSettingsWindowOpen()
	return SettingsWindowFrame ~= nil
end

function ClassicGuildBankExporter:OpenSettingsWindow()

	local textStore

	-- FIXME: Add a Info panel with a description to the window
	local frame = ClassicGuildBankExporterGUI:Create("Frame")
	frame:SetTitle("Configuration")
	frame:SetCallback("OnClose", function(widget)
			ClassicGuildBankExporter:CloseSettingsWindow() 
		end
	)
	frame:SetStatusText("Please enter your API Token.")
	frame:SetLayout("Flow")

	local editbox = ClassicGuildBankExporterGUI:Create("MultiLineEditBox")
	editbox:SetLabel("API Token:")
	editbox:SetWidth(400)
	editbox:SetNumLines(10)
	editbox:SetText(ApiToken)
	editbox:SetCallback("OnEnterPressed", function(widget, event, text) ClassicGuildBankExporter:SaveSettings(text) end)
	frame:AddChild(editbox)
	
	SettingsWindowFrame = frame
end

function ClassicGuildBankExporter:CloseSettingsWindow()
	-- FIXME: Find a better way to close the window
	ClassicGuildBankExporterGUI:Release(SettingsWindowFrame)
	SettingsWindowFrame = nil
end

function ClassicGuildBankExporter:SaveSettings(text)
	ApiToken = text
	ClassicGuildBankExporter:Print(text)
end

function ClassicGuildBankExporter:Reload()
	ReloadUI()
end

local extract = _G.bit32 and _G.bit32.extract
if not extract then
	if _G.bit then
		local shl, shr, band = _G.bit.lshift, _G.bit.rshift, _G.bit.band
		extract = function( v, from, width )
			return band( shr( v, from ), shl( 1, width ) - 1 )
		end
	elseif _G._VERSION >= "Lua 5.3" then
		extract = load[[return function( v, from, width )
			return ( v >> from ) & ((1 << width) - 1)
		end]]()
	else
		extract = function( v, from, width )
			local w = 0
			local flag = 2^from
			for i = 0, width-1 do
				local flag2 = flag + flag
				if v % flag2 >= flag then
					w = w + 2^i
				end
				flag = flag2
			end
			return w
		end
	end
end

local char, concat = string.char, table.concat

function ClassicGuildBankExporter:makeencoder( s62, s63, spad )
	local encoder = {}
	for b64code, char in pairs{[0]='A','B','C','D','E','F','G','H','I','J',
		'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y',
		'Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n',
		'o','p','q','r','s','t','u','v','w','x','y','z','0','1','2',
		'3','4','5','6','7','8','9',s62 or '+',s63 or'/',spad or'='} do
		encoder[b64code] = char:byte()
	end
	return encoder
end

function ClassicGuildBankExporter:EncodeExportString(exportString)
	encoder = ClassicGuildBankExporter:makeencoder()
	local t, k, n = {}, 1, #exportString
	local lastn = n % 3
	for i = 1, n-lastn, 3 do
		local a, b, c = exportString:byte( i, i+2 )
		local v = a*0x10000 + b*0x100 + c

		t[k] = char(encoder[extract(v,18,6)], encoder[extract(v,12,6)], encoder[extract(v,6,6)], encoder[extract(v,0,6)])
		k = k + 1
	end
	if lastn == 2 then
		local a, b = exportString:byte( n-1, n )
		local v = a*0x10000 + b*0x100
		t[k] = char(encoder[extract(v,18,6)], encoder[extract(v,12,6)], encoder[extract(v,6,6)], encoder[64])
	elseif lastn == 1 then
		local v = exportString:byte( n )*0x10000
		t[k] = char(encoder[extract(v,18,6)], encoder[extract(v,12,6)], encoder[64], encoder[64])
	end
	return concat( t )
end
