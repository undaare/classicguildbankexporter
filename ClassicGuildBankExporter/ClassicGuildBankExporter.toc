## Interface: 11302
## Title: ClassicGuildBankExporter
## Notes: Constructs ClassicGuildBank export strings
## Author: Crud, Undaare
## Version: 1.3
## SavedVariablesPerCharacter: BankExportString, ApiToken, GuildName

#@no-lib-strip@
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\AceGUI-3.0\AceGUI-3.0.xml
#@end-no-lib-strip@

ClassicGuildBankExporter.lua
